import java.util.Arrays;

public class SortowanieBabelkowe {

    public static int[] sortBabelkowe(int[] tablica) {

        int temp;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 1; j < (tablica.length - i); j++) {
                if (tablica[j - 1] > tablica[j]) {
                    temp = tablica[j - 1];
                    tablica[j - 1] = tablica[j];
                    tablica[j] = temp;

                }
            }
        }
        return tablica;
    }



    public static void main(String[] args) {

        int[] tablica = {5,2,4,6,1};

        System.out.println(Arrays.toString(sortBabelkowe(tablica)));


    }
}
