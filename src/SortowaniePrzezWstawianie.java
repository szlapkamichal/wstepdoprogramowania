public class SortowaniePrzezWstawianie {

    public static void sortowaniePrzezWstawienie(int[] wejscie){

        for (int j = 1; j < wejscie.length; j++) {
            int x = wejscie[j];
            int i = j;
            while ((i > 0) && (wejscie[i-1] > x)) {
                wejscie[i] = wejscie[i-1];
                i--;
            }
            wejscie[i] = x;
        }
    }


    public static int[] sortowaniePrzezWstawianieAlgorytm (int[] input){
        int x, i, j = input.length - 2;

        while (j >= 0) {
            x = input[j];
            i = j + 1;
            while ((i < input.length) && x> input[i]){
                input[i-1] = input[i];
                i++;
            }
            input[i-1] = x;
            j--;
        }
        return input;
    }


    public static void pokazTablice(int[] wejscie) {

        for(int x : wejscie) System.out.print (x + " ");
    }



    public static void main(String[] args) {

        int[] tablica = {8, 4, 7, 2, 9};

        pokazTablice(tablica);
        System.out.println();
        sortowaniePrzezWstawianieAlgorytm(tablica);
        pokazTablice(tablica);
        System.out.println();
        sortowaniePrzezWstawienie(tablica);

        pokazTablice(tablica);


    }
}
