public class NajwiekszyWspolnyDzielnik {

        public static void main(String[] args) {
            System.out.println(nwd(49, 21));
        }

        public static int nwd(int a, int b) {
            while (b != 0) {
                int temp = a;
                a = b;
                b = temp % b;
            }
            return a;
        }
    }