import java.util.Scanner;

public class WydawaniePieniedzy_DoZrobienia {
    // zaimplementuj algorytm, któy dla podanej kwoty poda w jaki sposób można wypłacać ją najmniejszą liczbą banknotów i montet.



    public static void main(String[] args) {

        final double[] NOMINALY = {500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};

        System.out.println("Podaj kwotę do wypłaty");

        Scanner scanner = new Scanner(System.in);

        double suma = scanner.nextDouble();
        double nominal = 0;
        double reszta = suma - nominal;

        String wynik = "Nominały: \n";

        for (int i = 0; i < NOMINALY.length; i++){
            if (reszta >= NOMINALY[i]) {
                int temp = (int)Math.floor(reszta/NOMINALY[i]);
                wynik = wynik + temp + " x " + NOMINALY[i] + " PLN \n";
                reszta = (double) Math.round(100 * (reszta - (temp * NOMINALY[i]))) / 100;
            }
        }
        System.out.println(wynik);
    }
}
