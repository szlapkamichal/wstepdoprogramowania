import java.util.Scanner;

public class Program {
    // napisać program do pokazania największego wspólnego dzielnika

    public static int nwdRekurencja(int liczbaA, int liczbaB) {
        if (liczbaB == 0) {
            return liczbaA;
        } else {
            return nwdRekurencja(liczbaB, liczbaA % liczbaB);
        }
    }

    public static int nwd (int liczbaA, int liczbaB){
        while (liczbaA != liczbaB){
            if (liczbaA > liczbaB){
                liczbaA = liczbaA - liczbaB;
            } else {
                liczbaB = liczbaB - liczbaA;
            }
        }
        return liczbaA;
    }

    public static int gcd2 (int a, int b){
        while (b !=0) {
            int temp = a;
            a = b;
            b = temp % b;
        }
        return a;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę:");
        int liczbaA = scanner.nextInt();
        System.out.println("Podaj drugą liczbę:");
        int liczbaB = scanner.nextInt();

        System.out.println("NWD to: " + nwd(liczbaA, liczbaB));
        System.out.println("NWD to: " + nwdRekurencja(liczbaA, liczbaB));
        System.out.println("NWD to: " + gcd2(liczbaA,liczbaB));
    }
}