import java.util.Arrays;
import java.util.Random;

public class WypisacTabliceWodwrotnejKolejnosci {

    private static Random random = new Random();

    public static int[] reverse(int[] tablica) {
        int[] d = new int[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            d[i] = tablica[tablica.length - 1 - i];
        }
        return d;
    }

    public static int[] reverse2(int[] array) {
        int temp;
        for (int i = 0; i < array.length / 2; i++) {
            temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        return array;
    }

    public static int[] firstToEnd(int[] in) {
        int temp = in[0];
        for (int i = 0; i < (in.length - 1); i++) {
            in[i] = in[i + 1];
        }
        in[in.length - 1] = temp;
        return in;
    }
    public static int[] endToFirst(int[] in) {
        int temp = in[0];
        for (int i = (in.length-1); i >= 0; i--) {
            in[i] = in[i + 1];
        }
        in[in.length - 1] = temp;
        return in;
    }

    public static String najdluzszyZTablicy (String[] array) {
        int maxLength = 0;
        String najdluzszyZTablicy = null;
        for (String s : array) {
            if (s.length() > maxLength) {
                maxLength = s.length();
                najdluzszyZTablicy = s;
            }
        }
        return najdluzszyZTablicy;
    }

    public static void main(String[] args) {

        String[] str = {"jeden", "dwa", "trzy"};
        System.out.println(najdluzszyZTablicy(str));

        int[] tablica = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        System.out.println(Arrays.toString(reverse(tablica)));

        String napis = "napis";
        System.out.println(napis.length());

    }
}
