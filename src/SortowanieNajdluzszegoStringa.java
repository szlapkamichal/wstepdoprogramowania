import java.util.Arrays;

public class SortowanieNajdluzszegoStringa {

    public static String[] sortBabelkowe(String[] tablica) {

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 1; j < (tablica.length - i); j++) {
                if (tablica[j - 1].length() > tablica[j].length()) {
                    String temp = tablica[j - 1];
                    tablica[j - 1] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
        return tablica;
    }
    public static void main(String[] args) {

        String[] tablica = {"ALA", "ma", "kota"};
        System.out.println(Arrays.toString(sortBabelkowe(tablica)));
    }
}
